#!/usr/bin/env bash

cd /vagrant

composer install -n

php bin/console server:start 0.0.0.0:8000