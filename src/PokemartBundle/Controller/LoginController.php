<?php
	namespace PokemartBundle\Controller;

	use PokemartBundle\Session\FlashBag\Level;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

	class LoginController extends PokemartController {
		/**
		 * @Template("PokemartBundle:Login:index.html.twig")
		 */
		public function indexAction() {
			$authUtils = $this->get('security.authentication_utils');
			$error = $authUtils->getLastAuthenticationError();

			if ($error)
				$this->get('session')->getFlashBag()->add(Level::WARNING, $error->getMessageKey());

			return [
				'username' => $authUtils->getLastUsername(),
			];
		}
	}