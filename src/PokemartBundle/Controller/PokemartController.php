<?php
	namespace PokemartBundle\Controller;

	use PokemartBundle\Entity\User;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;

	abstract class PokemartController extends Controller {
		/**
		 * @return User|null
		 */
		protected function getUser() {
			return parent::getUser();
		}
	}