<?php

namespace PokemartBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PokemartBundle extends Bundle
{
	public function boot() {
		date_default_timezone_set('UTC');
	}
}
