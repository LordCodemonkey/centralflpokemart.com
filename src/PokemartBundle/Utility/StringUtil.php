<?php
	namespace PokemartBundle\Utility;

	class StringUtil {
		/**
		 * @param string $str
		 *
		 * @return string
		 */
		public static function classify($str) {
			return str_replace(' ', '', ucwords(strtr($str, '_-', '  ')));
		}

		/**
		 * @param string $str
		 *
		 * @return string
		 */
		public static function camelize($str) {
			return lcfirst(self::classify($str));
		}

		/**
		 * @param string $str
		 *
		 * @return string
		 */
		public static function underscore($str) {
			$s = '';

			for ($i = 0, $ii = strlen($str); $i < $ii; $i++) {
				$c = $str[$i];

				if ($i > 0 && strtoupper($c) === $c)
					$c = '_' . $c;

				$s .= $c;
			}

			return strtolower($s);
		}
	}