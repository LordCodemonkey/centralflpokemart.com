<?php
	namespace PokemartBundle\Session\FlashBag;

	class Level {
		const SUCCESS = 'success';
		const INFO = 'info';
		const WARNING = 'warning';
		const DANGER = 'danger';

		private static $levels = [];

		public static function getLevels() {
			if (self::$levels)
				return self::$levels;

			$refl = new \ReflectionClass(self::class);

			self::$levels = $refl->getConstants();

			return self::$levels;
		}
	}