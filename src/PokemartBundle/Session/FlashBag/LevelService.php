<?php
	namespace PokemartBundle\Session\FlashBag;

	class LevelService {
		public function getLevels() {
			return Level::getLevels();
		}
	}