<?php
	namespace PokemartBundle\Entity;

	abstract class AbstractEntity {
		/**
		 * @return int|string|null
		 */
		public abstract function getId();

		public function __toString() {
			return sprintf('%s#%s', substr(static::class, strrpos(static::class, '\\') + 1), $this->getId() ?: '?');
		}
	}