<?php
	namespace PokemartBundle\Entity;

	class UserRole extends AbstractEntity {
		private $id;
		private $name;
		private $user;

		/**
		 * UserRole constructor.
		 *
		 * @param User   $user
		 * @param string $name
		 */
		public function __construct(User $user, $name) {
			$this->user = $user;
			$this->name = $name;
		}

		public function getId() {
			return $this->id;
		}

		/**
		 * @return string
		 */
		public function getName() {
			return $this->name;
		}

		/**
		 * @return User
		 */
		public function getUser() {
			return $this->user;
		}
	}