<?php
	namespace PokemartBundle\Entity;

	use Doctrine\Common\Collections\ArrayCollection;
	use Doctrine\Common\Collections\Collection;
	use Doctrine\Common\Collections\Criteria;
	use Doctrine\Common\Collections\Selectable;
	use Symfony\Component\Security\Core\User\UserInterface;

	class User extends AbstractEntity implements UserInterface, \Serializable {
		private $id;
		private $firstName;
		private $lastName;
		private $email;
		private $password;
		private $displayName;
		private $createdDate;

		/**
		 * @var Selectable|Collection|UserRole[]
		 */
		private $roles;

		public function __construct($firstName, $lastName, $email, $displayName) {
			$this->firstName = $firstName;
			$this->lastName = $lastName;
			$this->email = $email;
			$this->displayName = $displayName;
			$this->createdDate = new \DateTime();

			$this->roles = new ArrayCollection();
		}

		public function getId() {
			return $this->id;
		}

		/**
		 * @return string
		 */
		public function getFirstName() {
			return $this->firstName;
		}

		/**
		 * @param string $firstName
		 *
		 * @return $this
		 */
		public function setFirstName($firstName) {
			$this->firstName = $firstName;

			return $this;
		}

		/**
		 * @return string
		 */
		public function getLastName() {
			return $this->lastName;
		}

		/**
		 * @param string $lastName
		 *
		 * @return $this
		 */
		public function setLastName($lastName) {
			$this->lastName = $lastName;

			return $this;
		}

		/**
		 * @return string
		 */
		public function getFullName() {
			return trim(sprintf('%s %s', $this->getFirstName(), $this->getLastName()));
		}

		/**
		 * @return string
		 */
		public function getEmail() {
			return $this->email;
		}

		/**
		 * @param string $email
		 *
		 * @return $this
		 */
		public function setEmail($email) {
			$this->email = $email;

			return $this;
		}

		/**
		 * @return string
		 */
		public function getUsername() {
			return $this->getEmail();
		}

		/**
		 * @return string
		 */
		public function getPassword() {
			return $this->password;
		}

		/**
		 * @param string $password
		 *
		 * @return $this
		 */
		public function setPassword($password) {
			$this->password = $password;

			return $this;
		}

		/**
		 * @return string
		 */
		public function getDisplayName() {
			return $this->displayName;
		}

		/**
		 * @param string $displayName
		 *
		 * @return $this
		 */
		public function setDisplayName($displayName) {
			$this->displayName = $displayName;

			return $this;
		}

		/**
		 * @return \DateTime
		 */
		public function getCreatedDate() {
			return $this->createdDate;
		}

		/**
		 * @param Criteria|null $criteria
		 * @param bool          $simple
		 *
		 * @return string[]|Collection|Selectable|UserRole[]
		 */
		public function getRoles(Criteria $criteria = null, $simple = true) {
			$roles = $this->roles;

			if ($criteria)
				$roles = $roles->matching($criteria);

			if (!$simple)
				return $roles;

			return array_map(function(UserRole $role) {
				return $role->getName();
			}, $roles->toArray());
		}

		/**
		 * @param $name
		 *
		 * @return UserRole|null
		 */
		public function getRole($name) {
			$criteria = Criteria::create()
				->where(Criteria::expr()->eq('name', $name));

			$matching = $this->getRoles($criteria, false);

			if ($matching->count() === 0)
				return null;

			return $matching[0];
		}

		/**
		 * @param string $name
		 *
		 * @return bool
		 */
		public function hasRole($name) {
			return $this->getRole($name) !== null;
		}

		/**
		 * @param string $name
		 *
		 * @return UserRole
		 */
		public function addRole($name) {
			if ($role = $this->getRole($name))
				return $role;

			$this->getRoles(null, false)->add($role = new UserRole($this, $name));

			return $role;
		}

		/**
		 * @param string[] $names
		 *
		 * @return $this;
		 */
		public function addRoles(array $names) {
			foreach ($names as $name)
				$this->addRole($name);

			return $this;
		}

		/**
		 * @param string $name
		 *
		 * @return $this
		 */
		public function removeRole($name) {
			$role = $this->getRole($name);

			if (!$role)
				return $this;

			$this->getRoles(null, false)->removeElement($role);

			return $this;
		}

		/**
		 * @param string[] $names
		 *
		 * @return $this;
		 */
		public function setRoles(array $names) {
			foreach ($names as $name)
				$this->removeRole($name);

			return $this->addRoles($names);
		}

		public function getSalt() {
			// noop
		}

		public function eraseCredentials() {
			// noop
		}

		public function serialize() {
			return serialize([
				$this->getId(),
				$this->getUsername(),
				$this->getPassword(),
			]);
		}

		public function unserialize($serialized) {
			list($this->id, $this->email, $this->password) = unserialize($serialized);
		}
	}