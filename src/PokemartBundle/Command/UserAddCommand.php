<?php
	namespace PokemartBundle\Command;

	use Doctrine\ORM\EntityManagerInterface;
	use PokemartBundle\Entity\User;
	use PokemartBundle\Security\Role;
	use PokemartBundle\Utility\StringUtil;
	use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
	use Symfony\Component\Console\Input\InputArgument;
	use Symfony\Component\Console\Input\InputInterface;
	use Symfony\Component\Console\Input\InputOption;
	use Symfony\Component\Console\Output\OutputInterface;
	use Symfony\Component\Console\Question\ChoiceQuestion;
	use Symfony\Component\Console\Style\SymfonyStyle;

	class UserAddCommand extends ContainerAwareCommand {
		const E_NONE = 0;

		protected function configure() {
			parent
				::setName('app:user:add')
				->addArgument('firstName', InputArgument::REQUIRED)
				->addArgument('lastName', InputArgument::REQUIRED)
				->addArgument('email', InputArgument::REQUIRED)
				->addOption('displayname', 'd', InputOption::VALUE_OPTIONAL)
				->addOption('password', null, InputOption::VALUE_REQUIRED)
				->addOption('role', null, InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED);
		}

		protected function interact(InputInterface $input, OutputInterface $output) {
			$io = new SymfonyStyle($input, $output);

			foreach (['first name', 'last name', 'email'] as $name) {
				$key = StringUtil::camelize($name);

				if ($input->getArgument($key))
					continue;

				$input->setArgument($key, $io->ask('Please enter a ' . $name));
			}

			if (!$input->getOption('displayname'))
				$input->setOption('displayname', $io->ask('Please enter a display name (enter a dash for default)'));

			if (!$input->getOption('password'))
				$input->setOption('password', $io->askHidden('Please enter a password'));

			if (!$input->getOption('role')) {
				$q = new ChoiceQuestion('Enter one or more roles (comma separated)', array_keys(Role::getRoles()));
				$q->setMultiselect(true);

				$input->setOption('role', $io->askQuestion($q));
			}
		}

		protected function execute(InputInterface $input, OutputInterface $output) {
			/** @var EntityManagerInterface $em */
			$em = $this->getContainer()->get('doctrine')->getManager();
			$encoder = $this->getContainer()->get('security.password_encoder');

			$firstName = $input->getArgument('firstName');
			$lastName = $input->getArgument('lastName');
			$displayName = $input->getOption('displayname');

			if ($displayName === '-')
				$displayName = sprintf('%s%s.%s', $firstName, $lastName, uniqid());

			$user = new User($firstName, $lastName, $input->getArgument('email'), $displayName);

			$this->ensureUniqueFields($em, $user);

			$em->persist($user);

			$user->setPassword($encoder->encodePassword($user, $input->getOption('password')));

			foreach ($input->getOption('role') as $name)
				$user->addRole(Role::getRole($name));

			$em->flush();

			return self::E_NONE;
		}

		private function ensureUniqueFields(EntityManagerInterface $em, User $user) {
			$qb = $em->createQueryBuilder();
			$qb
				->select('COUNT(u)')
				->from('PokemartBundle:User', 'u')
				->where('u.email = :email')
				->orWhere('u.displayName = :display');

			$check = $qb
				->setParameter('email', $user->getEmail())
				->setParameter('display', $user->getDisplayName())
				->getQuery()
					->getSingleScalarResult();

			if ($check > 0)
				throw new \InvalidArgumentException('Could not create user. Please make sure it has a unique email ' .
					'and display name.');
		}
	}