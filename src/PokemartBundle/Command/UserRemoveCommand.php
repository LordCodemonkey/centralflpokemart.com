<?php
	namespace PokemartBundle\Command;

	use Doctrine\ORM\EntityManagerInterface;
	use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
	use Symfony\Component\Console\Input\InputArgument;
	use Symfony\Component\Console\Input\InputInterface;
	use Symfony\Component\Console\Output\OutputInterface;
	use Symfony\Component\Console\Style\SymfonyStyle;

	class UserRemoveCommand extends ContainerAwareCommand {
		const E_NONE = 0;
		const E_NOT_FOUND = 1;

		protected function configure() {
			parent
				::setName('app:user:remove')
				->addArgument('identifier', InputArgument::REQUIRED);
		}

		protected function interact(InputInterface $input, OutputInterface $output) {
			$io = new SymfonyStyle($input, $output);

			if (!$input->getArgument('identifier'))
				$input->setArgument('identifier', $io->ask('Please enter an identifier (username, email, ID, etc)'));
		}

		protected function execute(InputInterface $input, OutputInterface $output) {
			/** @var EntityManagerInterface $em */
			$em = $this->getContainer()->get('doctrine')->getManager();
			$io = new SymfonyStyle($input, $output);

			$qb = $em->createQueryBuilder();
			$qb
				->select('u')
				->from('PokemartBundle:User', 'u')
				->where('u.id = :ident')
				->orWhere('u.email = :ident')
				->orWhere('u.displayName = :ident');

			$user = $qb
				->setParameter('ident', $input->getArgument('identifier'))
				->getQuery()
				->getOneOrNullResult();

			if (!$user) {
				$io->error('No user found matching the identifier provided.');

				return self::E_NOT_FOUND;
			}

			if ($input->isInteractive() && !$io->confirm('Are you sure you want to delete ' . $user . '?', false)) {
				$io->text('Operation cancelled.');

				return self::E_NONE;
			}

			$em->remove($user);
			$em->flush();

			return self::E_NONE;
		}
	}