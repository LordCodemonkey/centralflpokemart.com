<?php
	namespace PokemartBundle\Twig\Functions;

	class IsActiveFunction extends \Twig_Extension {
		public function getName() {
			return 'app.twig.function.is_active';
		}

		public function getFunctions() {
			return [
				new \Twig_SimpleFunction('isActive', [$this, 'execute']),
			];
		}

		public function execute($route, $test, $beginsWith = false, $activeClass = 'active') {
			if ($beginsWith && stripos($test, $route) === false)
				return null;
			else if (strcasecmp($test, $route) !== 0)
				return null;

			return $activeClass;
		}
	}