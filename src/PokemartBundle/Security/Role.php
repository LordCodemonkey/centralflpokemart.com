<?php
	namespace PokemartBundle\Security;

	class Role {
		const ADMIN = 'ROLE_ADMIN';
		const USER = 'ROLE_USER';
		const EMPLOYEE = 'ROLE_EMPLOYEE';

		private static $roles = [];

		/**
		 * Returns an array of all known roles. The role name will be the key, and the role path will be the value.
		 *
		 * @return string[]
		 */
		public static function getRoles() {
			if (self::$roles)
				return self::$roles;

			$refl = new \ReflectionClass(self::class);

			self::$roles = $refl->getConstants();

			return self::$roles;
		}

		/**
		 * @param string $name
		 *
		 * @return string|null
		 */
		public static function getRole($name) {
			$roles = self::getRoles();

			if (!isset($roles[$name]))
				return null;

			return $roles[$name];
		}
	}